use crate::error::BitGenericError;
use crate::obj::Oid;
use crate::protocol::{BitProtocolRead, BitProtocolWrite};
use anyhow::Result;
use libbit::repo::BitRepo;
use std::str::FromStr;
use tokio::io;

// https://github.com/git/git/blob/master/Documentation/technical/protocol-common.txt
pub struct UploadPack<R, W> {
    repo: BitRepo,
    reader: R,
    writer: W,
}

// TODO on error send an error line
//   error-line     =  PKT-LINE("ERR" SP explanation-text)

// TODO
const CAPABILITIES: &[&str] = &[
    // "multi_ack",
    // "thin-pack",
    // "side-band",
    // "side-band-64k",
    // "ofs-delta",
    // "shallow",
    // "deepen-since",
    // "deepen-not",
    // "deepen-relative",
    // "no-progress",
    // "include-tag",
    // "multi_ack_detailed",
    // // TODO this one needs to actually read HEAD
    // "symref=HEAD:refs/heads/remote",
    // "object-format=sha1",
    // "agent=bit",
];

impl<R, W> UploadPack<R, W>
where
    R: BitProtocolRead,
    W: BitProtocolWrite,
{
    pub fn new(repo: BitRepo, reader: R, writer: W) -> Self {
        Self { repo, reader, writer }
    }

    #[tokio::main]
    pub async fn run(&mut self) -> Result<()> {
        self.write_ref_discovery().await?;
        let wanted = self.recv_wanted().await?;
        if wanted.is_empty() {
            return Ok(());
        }
        let has = self.recv_has().await?;
        dbg!(wanted);
        dbg!(has);
        Ok(())
    }

    fn parse_cmd_line<T>(&mut self, cmd: &str, packet: &[u8]) -> Result<T>
    where
        T: FromStr<Err = BitGenericError>,
    {
        let s = std::str::from_utf8(packet)?;
        let (c, oid) = s.split_once(' ').ok_or_else(|| anyhow!("bad `{}` line", cmd))?;
        ensure_eq!(c, cmd);
        oid.parse()
    }

    async fn recv_wanted(&mut self) -> Result<Vec<Oid>> {
        self.reader
            .recv_message()
            .await?
            .iter()
            .map(|packet| self.parse_cmd_line("want", packet))
            .collect()
    }

    async fn recv_has(&mut self) -> Result<Vec<Oid>> {
        self.reader
            .recv_message()
            .await?
            .iter()
            .map(|packet| self.parse_cmd_line("have", packet))
            .collect()
    }

    // Reference Discovery
    // -------------------
    //
    // When the client initially connects the server will immediately respond
    // with a version number (if "version=1" is sent as an Extra Parameter),
    // and a listing of each reference it has (all branches and tags) along
    // with the object name that each reference currently points to.
    //
    //    $ echo -e -n "0045git-upload-pack /schacon/gitbook.git\0host=example.com\0\0version=1\0" |
    //       nc -v example.com 9418
    //    000eversion 1
    //    00887217a7c7e582c46cec22a130adf4b9d7d950fba0 HEAD\0multi_ack thin-pack
    // 		side-band side-band-64k ofs-delta shallow no-progress include-tag
    //    00441d3fcd5ced445d1abc402225c0b8a1299641f497 refs/heads/integration
    //    003f7217a7c7e582c46cec22a130adf4b9d7d950fba0 refs/heads/master
    //    003cb88d2441cac0977faf98efc80305012112238d9d refs/tags/v0.9
    //    003c525128480b96c89e6418b1e40909bf6c5b2d580f refs/tags/v1.0
    //    003fe92df48743b7bc7d26bcaabfddde0a1e20cae47c refs/tags/v1.0^{}
    //    0000
    //
    // The returned response is a pkt-line stream describing each ref and
    // its current value.  The stream MUST be sorted by name according to
    // the C locale ordering.
    //
    // If HEAD is a valid ref, HEAD MUST appear as the first advertised
    // ref.  If HEAD is not a valid ref, HEAD MUST NOT appear in the
    // advertisement list at all, but other refs may still appear.
    //
    // The stream MUST include capability declarations behind a NUL on the
    // first ref. The peeled value of a ref (that is "ref^{}") MUST be
    // immediately after the ref itself, if presented. A conforming server
    // MUST peel the ref if it's an annotated tag.
    async fn write_ref_discovery(&mut self) -> Result<()> {
        let mut refs = self.repo.ls_refs()?.into_iter().collect::<Vec<_>>();
        // The order isn't really significant but keeping it close to git
        // The ord impl for refs is tailored for other purposes (i.e. remotes before heads in bit log)
        refs.sort_by_key(|r| r.path());
        for (i, r) in refs.into_iter().enumerate() {
            let oid = match self.repo.try_fully_resolve_ref(r)? {
                Some(oid) => oid,
                None => continue,
            };
            if i == 0 {
                self.write(format!("{} {}\0{}\n", oid, r.path(), CAPABILITIES.join(" "))).await?;
                continue;
            }
            self.write(format!("{} {}\n", oid, r.path()).as_bytes()).await?;
        }
        Ok(self.writer.write_flush_packet().await?)
    }

    #[inline]
    async fn write(&mut self, bytes: impl AsRef<[u8]>) -> io::Result<()> {
        self.writer.write_packet(bytes.as_ref()).await
    }
}
